# Namre this file solutions.py.
"""Volume II Lab 10: Fourier II (Filtering and Convolution)
<Name> James Longstaff
<Class> Math 321
<Date> 11/12/2015
"""

from Signal import Signal
import scipy as sp
from scipy.io import wavfile
import numpy as np

# Problem 1: Implement this function.
def clean_signal(outfile='prob1.wav'):
    """Clean the 'Noisysignal2.wav' file. Plot the resulting sound
    wave and write the resulting sound to the specified outfile.
    """
    rate, data = wavfile.read('Noisysignal2.wav')
    fourier_trans = sp.fft(data, axis = 0)
    size = len(data)/2
    for j in xrange(10000,size):
        fourier_trans[j] = 0
        fourier_trans[-j] = 0

    new_sig = sp.ifft(fourier_trans)
    new_sig = sp.real(new_sig)
    
    mysignal = Signal(rate,new_sig)
    mysignal.write_to_file(outfile)

    mysignal.plot()
    mysignal.plot(True)
    
    

# Problem 2 is not required. Use balloon.wav for problem 3.

# Problem 3: Implement this function.
def convolve(source='chopin.wav', pulse='balloon.wav', outfile='prob3.wav'):
    """Convolve the specified source file with the specified pulse file, then
    write the resulting sound wave to the specified outfile.
    """
    rate1,chopin_wave = wavfile.read(source)
    rate2, balloon_wave = wavfile.read(pulse)

    print chopin_wave.shape, balloon_wave.shape
    chopin_wave = chopin_wave[:,0]
    balloon_wave = balloon_wave[:,0]

    #Putting a few seconds on to the beginning of the file.
    zeros1 = np.zeros(10)
    chopin_wave = np.hstack((zeros1,chopin_wave))


    #print chopin_wave
    middle = len(balloon_wave)/2
    beginning = balloon_wave[:middle]
    end = balloon_wave[middle:]

    #This calculates the number of zeros you will add in the middle of balloon_wave. You will do this by calculating the difference between both of them.
    number_of_zeros = len(chopin_wave) - len(balloon_wave)

    print number_of_zeros
    zeros = np.zeros(number_of_zeros)
    print len(zeros)
    #Putting all of my pieces together.
    first_half = np.append(beginning,zeros)
    print len(first_half)
    modified_balloon = np.append(first_half,end)
    

    print len(beginning),len(end)
    print len(balloon_wave), number_of_zeros, len(modified_balloon)
    #print balloon_wave
    #print modified_balloon
    
    #Initializing my array
    

    print len(chopin_wave),len(modified_balloon)

    #Appending the convolution to the end of the convovled array which is actually empty.
    convolved = sp.fft(chopin_wave)*sp.fft(modified_balloon)
    
    #print convolved

    convolved_wave = np.fft.ifft(convolved)

    #initializing my modified wave function.
    piano_music = Signal(rate1,convolved_wave)
    
    #Finishing up
    piano_music.write_to_file(outfile)
    piano_music.plot()
    piano_music.plot(True)   
    

# Problem 4: Implement this function.
def white_noise(outfile='prob4.wav'):
    """Generate some white noise, write it to the specified outfile,
    and plot the spectrum (DFT) of the signal.
    """
    
    samplerate = 22050

    noise = sp.int16(sp.random.randint(-32767, 32767, samplerate*10))
    noisy_signal = Signal(samplerate*10,noise)
    
    noisy_signal.write_to_file(outfile)
    noisy_signal.plot()
    noisy_signal.plot(True)
    
    

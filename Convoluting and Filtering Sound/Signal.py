# name this file solutions.py
"""Volume II Lab 9: Discrete Fourier Transform
<Name> James Lonsgtaff
<Class> Math 321
<Date> 11/12/2015
"""
import numpy as np
import matplotlib.pyplot as plt
from scipy.io import wavfile
import scipy as sp

# Modify this class for problems 1, 2, 4, and 5.
class Signal(object):
    
    def __init__(self,sampleRate = 44100,samples = np.array([1,2,3,4])):
        self.sr = sampleRate
        self.samples = samples
        self.dft  = DFT(samples)

    def __add__(self,other):
        return Signal(self.sr,self.samples + other.samples)
            
    
    def plot(self, DFT_status = False):
        if DFT_status is False:
            plt.plot(self.samples)
            plt.show()
        else:
            plt.plot(self.dft)
            plt.show()
            

    def write_to_file(self, filename = 'my_sound.wav'):
        samples = self.samples
        samples = sp.int16(samples / sp.absolute(samples).max() * 32767)
        wavfile.write(filename,self.sr, samples)
        
def wave_function(x,frequency):
    return np.sin(2*np.pi*x*frequency)

def read_from_file(filename = 'tada_mono.wav'):
    rate,wave = wavfile.read(filename)
    return rate, wave

# Problem 3: Implement this function.
def generate_note(frequency = 440, duration = 5):
    """Return an instance of the Signal class corresponding to the desired
    soundwave. Sample at a rate of 44100 samples per second.
    """
    samplerate = 44100
    stepsize = 1./ samplerate
    sample_points = np.arange(0, duration, stepsize)
    samples = wave_function(sample_points,frequency)   
    return Signal(samplerate,samples)
    

# Problem 4: Implement this function.
def DFT(samples):
    """Calculated the DFT of the given array of samples."""
    return sp.fft(samples)
    

# Problem 6: Implement this function.
def generate_chord():
    """Write a chord to a new file, 'chord1.wav'. Write a chord that changes
    over time to a new file, 'chord2.wav'.
    """
    A = generate_note(440,5)
    B = generate_note(493.88,5)
    C = generate_note(523.25,5)
    D = generate_note(587.33,5)
    E = generate_note(659.25,5)
    F = generate_note(698.46,5)
    G = generate_note(783.99,5)
    sfd = generate_note(16.35,5)


    """
    chord_samples = (A.samples, C.samples, E.samples)
    print chord_samples
    chord = np.hstack(chord_samples)
    print chord
    """
    '''
    A.samples = np.hstack(A.samples)
    C.samples = np.hstack(C.samples)
    E.samples = np.hstack(E.samples)
    '''
    chord = A+C+E
    
    
    chord.write_to_file('chord1.wav')
    
    A.write_to_file('A.wav')
    C.write_to_file('C.wav')
    E.write_to_file('E.wav')
    sfd.write_to_file('sfd.wav')

    mix = Signal(chord.sr,np.hstack((A.samples,C.samples,chord.samples)))

    mix.write_to_file('chord2.wav')

    #A.plot(True)
    #C.plot(True)
    #E.plot(True)
    
    chord.plot(True)
    mix.plot(True)
    mix.plot(False)
    
    
    

    

def test():
    #array = np.array([-0.5,1,0.5,-0.5,1,0.5,-0.5,1,0.5,-0.5,1,0.5,-0.5,1,0.5,-0.5,1,0.5,-0.5,1,0.5])
    #rate, wave = read_from_file('Noisysignal2.wav')
    
    #mysignal = Signal(rate,wave)
    #print str(mysignal)
    
    generate_chord()
    #mysignal.write_to_file()
    mysignal.plot(True)
    #mysignal2 = generate_note()
    #mysignal2.plot(True)
    #mysignal.write_to_file('A_test.wav')

    

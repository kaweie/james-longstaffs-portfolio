#Implement this function
import numpy as np
import matplotlib.pyplot as plt
from numpy.random import randn
from numpy.random import normal
import matplotlib.patches as mpatches


def problemOne():
    '''
    Order and plot the data as a horizontal bar chart.
    '''
    male = np.array([179.2,160.0,177.8,178.9,178,176,172.5,165.6,170.8,183.2,182.4,164,163.6,175.4,174,176.1,165.7])
    female = np.array([167.6,142.2,164.5,165.3,165,164,158,154.9,157.4,168.4,168,151,151.4,164,158.9,162.1,155.2])
    country = 'Austria', 'Bolivia', 'England', 'Finland', 'Germany', 'Hungary', 'Japan','North Korea', 'South Korea', 'Montenegro', 'Norway', 'Peru','Sri Lanka', 'Switzerland', 'Turkey', 'U.S.', 'Vietnam'

    pos = np.arange(17)+.5
    
    #Plot Men
    plt.barh(pos,male,align = 'center', color = 'r')
    plt.yticks(pos, country[::1])
    plt.barh(pos,female,align = 'center', color = 'b')
    plt.yticks(pos, country[::1])
    blue_patch = mpatches.Patch(color='blue', label='Women')
    red_patch = mpatches.Patch(color='red', label='Men')
    plt.legend(handles=[blue_patch,red_patch], loc = 2)
    plt.title("Heights of Men and Women from Different Countries")
    plt.show()

    #Plot Women

    #plt.show()
    #raise NotImplementedError("Problem 1 not implemented.")

# Implement this function
def problemTwo():
    '''
    Plot some histograms with white reference lines.  Do this for 20 bins,
    10 bins, 5 bins, and 3 bins
    '''

    #data = randn(75) + randn(75) + randn(75) + randn(75)
    
    normal_numbers = normal(size = 1000)
    """
    plt.hist(normal_numbers,100)#data)
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("100 Bins")
    plt.show()
    """
    
    plt.hist(normal_numbers,20)#data)
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("20 Bins")
    plt.show()

    
    plt.hist(normal_numbers,10)#data)
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("10 Bins")
    plt.show()

    
    plt.hist(normal_numbers,5)#data)
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("5 Bins")
    plt.show()

    
    plt.hist(normal_numbers,3)#data)
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("3 Bins")
    plt.show()
    #raise NotImplementedError("Problem 2 not implemented.")

# Implement this function
def problemThree():
    '''
    Plot y = x^2 * sin(x) using 1000 data points and x in [0,100]
    '''
    x = np.linspace(0,100,1000)
    y = (x**2)*(np.sin(x))
    line = plt.plot(x,y)
    plt.setp(line, linewidth = 2, color = 'b')
    plt.show()
    #raise NotImplementedError("Problem 3 not implemented.")

# Implement this function
def problemFour():
    '''
    Plot a scatter plot of the average heights of men against women
    using the data from problem 1.
    '''
    male = np.array([179.2,160.0,177.8,178.9,178,176,172.5,165.6,170.8,183.2,182.4,164,163.6,175.4,174,176.1,165.7])
    female = np.array([167.6,142.2,164.5,165.3,165,164,158,154.9,157.4,168.4,168,151,151.4,164,158.9,162.1,155.2])
    country = 'Austria', 'Bolivia', 'England', 'Finland', 'Germany', 'Hungary', 'Japan','North Korea', 'South Korea', 'Montenegro', 'Norway', 'Peru','Sri Lanka', 'Switzerland', 'Turkey', 'U.S.', 'Vietnam'

    plt.scatter(male,female)
    plt.title("Scatter Plot of Heights")
    plt.show()
    #raise NotImplementedError("Problem 4 not implemented.")
    
# Implement this function
def problemFive():
    '''
    Plot a contour map of z = sin(x) + sin(y) where x is in [0,12*pi] and
    y is in [0,12*pi]
    '''
    n = 400
    x = np.linspace(0.0,12*np.pi,n)
    y = np.linspace(0.0,12*np.pi,n)
    
    X,Y = np.meshgrid(x,y)
    z = np.sin(X) + np.sin(Y)
    plt.contourf(X,Y,z,[-2,-1,0.0001,1,2,3,4,5],cmap=plt.get_cmap('afmhot'))
    plt.show()
    #raise NotImplementedError("Problem 5 not implemented.")
    
# Implement this function
def problemSix():
    '''
    Plot each data set.
    '''
    dataI = np.array([[10,8.04],[8.,6.95],[13.,7.58],[9,8.81],[11.,8.33],[14.,9.96],[6.,7.24],[4.,4.26],[12.,10.84],[7.,4.82],[5.,5.68]])
    dataII = np.array([[10,9.14],[8.,8.14],[13.,8.74],[9,8.77],[11.,9.26],[14.,8.10],[6.,6.13],[4.,3.10],[12.,9.13],[7.,7.26],[5.,4.74]])
    dataIII = np.array([[10,7.46],[8.,6.77],[13.,12.74],[9,7.11],[11.,7.81],[14.,8.84],[6.,6.08],[4.,5.39],[12.,8.15],[7.,6.42],[5.,5.73]])
    dataIV = np.array([[8.,6.58],[8.,5.76],[8.,7.71],[8.,8.84],[8.,8.47],[8.,7.04],[8.,5.25],[19.,12.50],[8.,5.56],[8.,7.91],[8.,6.89]])
    
    plt.scatter(dataI[:,0],dataI[:,1])#, cmap = 'afmhot')
    plt.show()
    plt.scatter(dataII[:,0],dataII[:,1],)#, cmap = 'afmhot')
    plt.show()
    plt.scatter(dataIII[:,0],dataIII[:,1])#, cmap = 'afmhot')
    plt.show()
    plt.scatter(dataIV[:,0],dataIV[:,1])#, cmap = 'afmhot')
    plt.show()

    

# Implement this function
def problemSeven():
    '''
    Change the surface to a heatmap or a contour plot.  Return a string
    of the benefits of each type of visualization.
    '''
    x = np.linspace(-2*np.pi,2*np.pi,num=400)
    y = np.linspace(-2*np.pi,2*np.pi,num=400)
    X, Y = np.meshgrid(x,y)
    Z = np.exp(np.cos(np.sqrt(X**2 + Y**2)))
    fig = plt.figure()

    #Contour Map
    plt.contour(X, Y, Z, [-2,-1,0.0001,1,2,3,4,5] ,cmap=plt.get_cmap('afmhot'))
    plt.title("Contour Map")
    plt.show()

    plt.contourf(X, Y, Z,[-2,-1,0.0001,1,2,3,4,5],cmap=plt.get_cmap('afmhot'))
    plt.title("Heat Map")
    plt.show()
    
    return "A heat map shows location of a certain level, while a contour map shows more specifically where states and level change."
    #raise NotImplementedError("Problem 7 not implemented.")

# Implement this function
def problemEight():
    '''
    Plot y = x^2 * sin(x) where x is in [0,100] and adjust to y limit to be
    [-10^k,10^k] for k = 0,1,2,3,4.
    '''
    print "Expect 4 figures"
    for i in xrange(5):
        x = np.linspace(0,100,1000)
        y = (x**2)*(np.sin(x))
        line = plt.plot(x,y)
        plt.setp(line, linewidth = 2, color = 'b')
        plt.ylim(-10**i,10**i)
        plt.show()
       
# Implement this function
def problemNine():
    '''
    Simplify one of your previous graphs.
    '''
    normal_numbers = normal(size = 1000)

    plt.hist(normal_numbers,20)#data)
    
    
    
    axis = plt.gca()
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)
    axis.yaxis.set_ticks_position('left')
    axis.xaxis.set_ticks_position('bottom')
    plt.xticks(np.arange(-4,4,0.5))
    plt.grid(True, color = 'w', linestyle = '-')
    plt.title("20 Bins")
    
    plt.show()
    
    
    plt.hist(normal_numbers,10)#data)
    
    axis = plt.gca()
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)
    axis.yaxis.set_ticks_position('left')
    axis.xaxis.set_ticks_position('bottom')
    plt.xticks(np.arange(-4,4,0.5))

    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("10 Bins")
    plt.show()

    
    plt.hist(normal_numbers,5)#data)
    axis = plt.gca()
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)
    axis.yaxis.set_ticks_position('left')
    axis.xaxis.set_ticks_position('bottom')
    plt.xticks(np.arange(-4,4,0.5))
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("5 Bins")
    plt.show()

    
    plt.hist(normal_numbers,3)#data)
    axis = plt.gca()
    axis.spines['right'].set_visible(False)
    axis.spines['top'].set_visible(False)
    axis.yaxis.set_ticks_position('left')
    axis.xaxis.set_ticks_position('bottom')
    plt.xticks(np.arange(-4,4,0.5))
    plt.grid(True, color = 'w', linestyle = '-')
    
    plt.title("3 Bins")
    plt.show()
    #raise NotImplementedError("Problem 2 not implemented.")
   

# Implement this function
def problemTen():
    '''
    Plot the Bernstein polynomials for v,n in [0,3] as small multiples
    and as the cluttered version.
    '''
    raise NotImplementedError("Problem 10 not implemented.")

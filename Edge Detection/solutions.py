import numpy as np
from numpy import linalg as la
from scipy import signal as sig
from matplotlib import pyplot as plt
# Problem 1: Implement this function.
def centered_difference_quotient(f,pts,h = 1e-5):
    '''
    Compute the centered difference quotient for function (f)
    given points (pts).
    Inputs:
        f (function): the function for which the derivative will be approximated
        pts (array): array of values to calculate the derivative
    Returns:
        centered difference quotient (array): array of the centered difference
            quotient
    '''
    return (f(pts+h) - f(pts-h))/(2*h)



# Problem 2: Implement this function.
def jacobian(f,n,m,pt,h = 1e-5):
    '''
    Compute the approximate Jacobian matrix of f at pt using the centered
    difference quotient.
    Inputs:
        f (function): the multidimensional function for which the derivative
            will be approximated
        n (int): dimension of the domain of f
        m (int): dimension of the range of f
        pt (array): an n-dimensional array representing a point in R^n
        h (float): a float to use in the centered difference approximation
    Returns:
        Jacobian matrix of f at pt using the centered difference quotient.
    '''
    j_matrix = np.ones((n,m), dtype = float)
    

    """
    for j in xrange(len(f)):
        partial = []
        for point in pt:
            partial.append((f[j](point +h) - f[j](point))/h)

        partial = np.array(partial)
        j_matrix[:,j] = partial
    
    return j_matrix
    """

    e = np.eye(n)

    for j in xrange(n):
        #print pt + h*e[:,j]
        first_input = pt +h*e[:,j]
        second_input = pt - h*e[:,j]
        
        #print first_input
        #print second_input
        j_matrix[:,j] = (f(first_input) - f(second_input))/(2*h)
    
    return j_matrix
    


# Problem 3: Implement this function.
def findError():
    '''
    Compute the maximum error of your jacobian function for the function
    f(x,y)=[(e^x)*sin(y)+y^3,3y-cos(x)] on the square [-1,1]x[-1,1].
    Returns:
        Maximum error of your jacobian function.
    '''
    errors = []
    domain_pts = np.linspace(-1,1,100)
    range_pts = np.linspace(-1,1,100)

    for i in domain_pts:
        #print i
        for j in range_pts:
            #print i
            #print j
            f = lambda x: np.array([(np.e**j)*np.sin(i) + i**3, 3*i - np.cos(j)])
            Jacobian_matrix = jacobian(f,2,2,np.array([j,i]))

            calculated_matrix = np.array([[(np.e**i)*np.sin(j),(np.e**j)*np.cos(i)+3*i**2],[ np.sin(j) , 3 ]])

            errors.append(la.norm(Jacobian_matrix - calculated_matrix))

    return max(errors)
        
    
        
# Problem 4: Implement this function.
def Filter(image,F):
    '''
    Applies the filter to the image.
    Inputs:
        image (array): an array of the image
        F (array): an nxn filter to be applied (a numpy array).
    Returns:
        The filtered image.
    '''
    #image = image[:,:,1]
    m, n = image.shape
    h, k = F.shape
    image_pad = np.zeros((m+(2*(h/2)), n+(2*(k/2))))
    print image_pad.shape
    print image.shape
    print F.shape
   
    image_pad[h/2:(h/2 + m), k/2:(k/2 + n)] = image
    #print F
    C = np.zeros(image.shape)
    
    """
    print '\n'
    print n
    print '\n'
    print m
    """
    print "Is this the problem?"
    #C = sig.convolve(F,image)    

    for i in range(m):
        print i
        for j in range(n):
            C[i,j] = np.trace(np.dot(F.T,image_pad[i:i+h,j:j+k]))             
    
    
    return C

# Problem 5: Implement this function.
def sobelFilter(image):
    '''
    Applies the Sobel filter to the image
    Inputs:
        image(array): an array of the image in grayscale
    Returns:
        The image with the Sobel filter applied.
    '''
    m,n = image.shape
    
    
    S = (1/8.0)* np.array([[-1,0,1],[-2,0,2],[-1,0,1]])
    #flat_image = image[:,:,1]
    
    first = Filter(image,S)
    second = Filter(image,S.T)
    
    Euclid_of_gradA = np.sqrt(first**2 + second**2)
    #B = np.sqrt(first**2 + second**2)
    #print Euclid_of_gradA.shape()
    #print B.mean()
    #M = 4.0*Euclid_of_gradA.mean()
    M = Euclid_of_gradA.mean()*4.0  
    #print M
    
    B = np.ones((m,n))
        
    B[Euclid_of_gradA <= M] = 0
    #print Euclid_of_gradA
    print B
    #print B.shape()
    #print B.size
    #print Euclid_of_gradA.size
    
    #B[B > M] = 1.0
    #B[B <= M] = 0.0
    
    return B
    """
    plt.imshow(Euclid_of_gradA, cmap = 'afmhot')
    plt.show(Euclid_of_gradA)
    """
def test():

    f = lambda x: -5*x**3 + 4*x**2 - 17*x
    test_array = np.array([-1.0,1.0,9.0,np.pi])
    return jacobian(f,4,3,np.array([1,2,3,4]),test_array)
    
